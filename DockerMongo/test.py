from flask_testing import TestCase
import unittest
from app import app, db



class ButtonTest(TestCase):

    def create_app(self):
        app.config["Testing"] = True
        return app

    def test_no_input(self):
        """
        A 302 status code will be returned when a request with no input is sent to  /store
        """
        tc = app.test_client()
        x = (tc.get("/store?distance=200&begin_date=2021-11-04&begin_time=00%3A00&action2=Submit&miles=&km=&location=&open=&close=&miles=&km=&location=&open=&close=&miles=&km=&location=&open=&close=&miles=&km=&location=&open=&close=&miles=&km=&location=&open=&close=&miles=&km=&location=&open=&close=&miles=&km=&location=&open=&close=&miles=&km=&location=&open=&close=&miles=&km=&location=&open=&close=&miles=&km=&location=&open=&close=&miles=&km=&location=&open=&close=&miles=&km=&location=&open=&close=&miles=&km=&location=&open=&close=&miles=&km=&location=&open=&close=&miles=&km=&location=&open=&close=&miles=&km=&location=&open=&close=&miles=&km=&location=&open=&close=&miles=&km=&location=&open=&close=&miles=&km=&location=&open=&close=&miles=&km=&location=&open=&close="))
        assert x.status_code == 302

    def test_empty_db(self):
        """
        If a database is empty it will redirect to an error page (302) when display 
        is clicked. If it is not empty it'll render a template containing the times stored in the data base (200)      
        """
        tc = app.test_client()
        x = (tc.get("/new?distance=200&begin_date=2021-11-04&begin_time=00%3A00&action2=Submit&miles=&km=&location=&open=&close=&miles=&km=&location=&open=&close=&miles=&km=&location=&open=&close=&miles=&km=&location=&open=&close=&miles=&km=&location=&open=&close=&miles=&km=&location=&open=&close=&miles=&km=&location=&open=&close=&miles=&km=&location=&open=&close=&miles=&km=&location=&open=&close=&miles=&km=&location=&open=&close=&miles=&km=&location=&open=&close=&miles=&km=&location=&open=&close=&miles=&km=&location=&open=&close=&miles=&km=&location=&open=&close=&miles=&km=&location=&open=&close=&miles=&km=&location=&open=&close=&miles=&km=&location=&open=&close=&miles=&km=&location=&open=&close=&miles=&km=&location=&open=&close=&miles=&km=&location=&open=&close="))
        if db.tododb.count() == 0:
            assert x.status_code == 302
        else:
            assert x.status_code == 200


if __name__ == "__main__":
    unittest.main()